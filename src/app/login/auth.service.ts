import { Injectable } from '@angular/core';
import { Router } from "@angular/router";

import { Usuario } from './usuario';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private usuarioAutenticado: boolean = false;

  constructor(private router: Router) { }

  fazerLogin(usuario: Usuario) {
    if (usuario.nome === 'maxine' &&
      usuario.senha === '12345') {

          this.usuarioAutenticado = true;
          localStorage['token'] = 'xptoh26410x5=50';
          this.router.navigate(['/agenda']);

      } else {
        this.usuarioAutenticado = false;
      }
  }

  usuarioEstaAutenticado(){
    return this.usuarioAutenticado;
  }

}

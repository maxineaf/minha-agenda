import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
//import { Validators } from '@angular/forms';
import { AuthService } from './auth.service';
import { Usuario } from './usuario';


@Component ({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
    
    autenticado = false;

    private usuario: Usuario = new Usuario();

    constructor(private authService: AuthService) {}

    ngOnInit() {

    }

    fazerLogin(form: NgForm) {        
        this.authService.fazerLogin(this.usuario);
        this.autenticado = true;
    }
}

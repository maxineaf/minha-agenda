import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-message',
  template: `
    <div *ngIf="temErro()" class="message-error">
      <small>{{ text }}</small>
    </div>
  `,
  styles: [`
    .message-error {
      color: rgb(204, 12, 12);
    }
  `]
})
export class MessageComponent {

  @Input() error: string;
  @Input() control: FormControl;
  @Input() text: string;

  temErro() : boolean {
    return this.control.hasError(this.error) && this.control.touched;
  }

}

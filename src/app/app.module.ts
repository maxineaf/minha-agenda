import { AuthGuard } from './service/auth.guard';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from '../environments/environment';

import {MatMenuModule} from '@angular/material/menu';
import {MatTableModule} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AddComponent } from './add/add.component';
import { ContatoComponent } from './contato/contato.component';

import { AppRoutingModule } from './app-routing.module';
import { AuthService } from './login/auth.service';
import { routing } from './app.routing';
import { AgendaComponent } from './agenda/agenda.component';
import { MessageComponent } from './message/message.component';
import { AgendaGridComponent } from './agenda-grid/agenda-grid.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Error404Component } from './error404/error404.component';

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'agenda', component: AddComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AddComponent,
    ContatoComponent,
    AgendaComponent,
    MessageComponent,
    AgendaGridComponent,
    Error404Component
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
     AngularFirestoreModule,
     AngularFireAuthModule,
     AngularFireDatabaseModule,
    BrowserModule,
    ReactiveFormsModule, 
    AppRoutingModule,
    FormsModule,
    MatMenuModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    routing,
    RouterModule.forRoot (
      appRoutes,
      { enableTracing: true }
    ),
    BrowserAnimationsModule
  ],
  providers: [
    AuthService,
    AuthGuard
  ],
  
  bootstrap: [AppComponent]
})
export class AppModule { }

import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddComponent } from './add/add.component';
import { LoginComponent } from './login/login.component';
import { AgendaComponent } from './agenda/agenda.component';
import { Error404Component } from './error404/error404.component';
import { AuthGuard } from './service/auth.guard';

const APP_ROUTES: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'agenda', component: AgendaComponent, canActivate: [AuthGuard] },
    { path: 'add', component: AddComponent, canActivate: [AuthGuard] },
    { path: '**', component: Error404Component },
    { path: '', redirectTo: 'agenda', pathMatch: 'full' }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);
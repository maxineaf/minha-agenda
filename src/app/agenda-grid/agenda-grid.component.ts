import { ContatoDataService } from './../contato/contato-data.service';
import { ContatoService } from './../contato/contato.service';
import { ContatoComponent } from './../contato/contato.component';
import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-agenda-grid',
  templateUrl: './agenda-grid.component.html',
  styleUrls: ['./agenda-grid.component.css']
})
export class AgendaGridComponent {

  constructor(private contatoService: ContatoService, private contatoDataService: ContatoDataService) {}

  agenda = [
    {nome: 'Mark', telefone: '0099999999', dataNascimento: new Date(1993, 4, 6), email: 'mark_exemplo@EmailValidator.com', relacionamento: 'familia'},
    {nome: 'Jack', telefone: '0099999999', dataNascimento: new Date(1994, 8, 30), email: 'jack_exemplo@EmailValidator.com', relacionamento: 'amigo'},
    {nome: 'Jim', telefone: '0099999999', dataNascimento: new Date(1995, 2, 16), email: 'jim_exemplo@EmailValidator.com', relacionamento: 'amigo'},
    {nome: 'Robin', telefone: '0099999999', dataNascimento: new Date(1990, 5, 28), email: 'robin_exemplo@EmailValidator.com', relacionamento: 'trabalho'},
    {nome: 'Emily', telefone: '0099999999', dataNascimento: new Date(1989, 12, 21), email: 'emily_exemplo@EmailValidator.com', relacionamento: 'conhecido'},
    {nome: 'Anna', telefone: '0099999999', dataNascimento: new Date(1997, 6, 16), email: 'anna_exemplo@EmailValidator.com', relacionamento: 'vizinho'},
    {nome: 'Robert', telefone: '0099999999', dataNascimento: new Date(1991, 10, 19), email: 'robert_exemplo@EmailValidator.com', relacionamento: 'familia'},
    {nome: 'Yasmim', telefone: '0099999999', dataNascimento: new Date(1996, 11, 3), email: 'yasmim_exemplo@EmailValidator.com', relacionamento: 'amigo'},
    {nome: 'Sthefannie', telefone: '0099999999', dataNascimento: new Date(1993, 3, 22), email: 'sthefannie_exemplo@EmailValidator.com', relacionamento: 'trabalho'},
    {nome: 'Nast', telefone: '0099999999', dataNascimento: new Date(1993, 7, 10), email: 'nast_exemplo@EmailValidator.com', relacionamento: 'amigo'}
  ];

  displayedColumns: string[] = ['nome', 'telefone', 'dataNascimento', 'email', 'relacionamento', 'acao'];
  dataSource = new MatTableDataSource(this.agenda);

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  edit(contato: ContatoComponent, key: string) {
    this.contatoDataService.changeContato(contato, key);
  }

}

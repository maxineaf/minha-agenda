import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, NgForm } from '@angular/forms';
//import { convertMetaToOutput } from '@angular/compiler/src/render3/util';
//import { Validators } from '@angular/forms';

import { ContatoComponent } from '../contato/contato.component';
import { ContatoService } from '../contato/contato.service';
import { ContatoDataService } from '../contato/contato-data.service';


@Component ({
    selector: 'app-add',
    templateUrl: './add.component.html',
    styleUrls: ['./add.component.css']
})

export class AddComponent implements OnInit{
    contato: ContatoComponent;
    key: string = '';
    adicionado = false;

    constructor(private contatoService: ContatoService,
                private contatoDataService: ContatoDataService){}
    
    ngOnInit() {
        this.contato = new ContatoComponent();
        this.contatoDataService.currentContato.subscribe(data => {
          if (data.contato && data.key) {
            this.contato = new ContatoComponent();
            this.contato.nome = data.contato.nome;
            this.contato.telefone = data.contato.telefone;
            this.contato.dataNascimento = data.contato.dataNascimento;
            this.contato.email = data.contato.email;
            this.contato.relacionamento = data.contato.relacionamento;
            this.key = data.key;
          }
        })
      }

    onSubmit() {
        if (this.key) {
            this.contatoService.update(this.contato, this.key);
          } else {
            this.contatoService.insert(this.contato);
            this.adicionado = true;
          }
          this.contato = new ContatoComponent();
        }
       
}
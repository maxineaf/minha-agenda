// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDG6m-X0rQxbFjKfqx_wxJ0nTjX25sQqg0",
    authDomain: "minha-agenda-1c870.firebaseapp.com",
    databaseURL: "https://minha-agenda-1c870.firebaseio.com",
    projectId: "minha-agenda-1c870",
    storageBucket: "minha-agenda-1c870.appspot.com",
    messagingSenderId: "546513742177",
    appId: "1:546513742177:web:b7c5d35d851d3e61b13d20",
    measurementId: "G-QBLV24Y03J"
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
